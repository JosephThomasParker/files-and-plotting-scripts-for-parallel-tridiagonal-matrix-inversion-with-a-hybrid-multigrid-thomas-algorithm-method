import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pandas as pd
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from matplotlib import rc

rc("font", **{"family": "DejaVu Sans", "serif": ["Times"]})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc("text", usetex=True)


def make_plot_dir(args):
    if args.log_dir is not None:
        plot_dir = "plots/" + args.log_dir + "/"
    else:
        plot_dir = "plots/" + args.cyclic + "/"
    return plot_dir


def sub_plot_cyclic(cdf, timer):
    """Plot the data from cyclic runs"""
    ax = cdf.sort_values("NXPE").plot(
        x="NXPE",
        y=timer,
        ax=plt.gca(),
        marker="+",
        lw=0,
        color="r",
        label="",
    )
    return ax


def sub_plot_cyclic_average(cdfa, timer):
    """Plot the averaged data from cyclic runs"""
    ax = cdfa.sort_values("NXPE").plot(
        x="NXPE",
        y="mean_" + timer,
        ax=plt.gca(),
        marker="s",
        markersize=4,
        lw=1,
        color="r",
        label="cyclic",
        linestyle="--",
    )
    ###    ax.fill_between(
    ###        cdfa.sort_values(by="NXPE").NXPE,
    ###        cdfa.sort_values(by="NXPE")["ci_down_" + timer],
    ###        cdfa.sort_values(by="NXPE")["ci_up_" + timer],
    ###        color="r",
    ###        alpha=0.2,
    ###        lw=0,
    ###    )
    return ax


def sub_plot_ipt_time_vs_nxpe_fixed_max_level(
    d, max_level, colorstr, i, timer="run_time"
):
    ax = d.sort_values("NXPE").plot(
        x="NXPE",
        y=timer,
        ax=plt.gca(),
        marker="x",
        lw=0,
        color=[colorstr[i]],
        label="",
    )

    # Set true to print timing values
    if False:
        print(d.sort_values("NXPE")[timer])

    return ax


def sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(
    nxpe,
    d,
    colorstr,
    timer="run_time",
    subideal=None,
    pointstyle="o",
    labelstr="multigrid-Thomas",
    ideal=True,
    linestyle="-",
):
    plt.plot(
        nxpe,
        d,
        marker=pointstyle,
        markersize=4,
        lw=1,
        color=colorstr,
        label=labelstr,
        linestyle=linestyle,
    )

    # Set true to print timing data
    if False:
        print(d)

    if ideal:
        plt.plot(
            nxpe,
            d[0] * nxpe[0] / nxpe,
            marker="",
            lw=0.5,
            linestyle="-",
            color="k",
            label="ideal",
        )

        # Set true to print timing data
        if False:
            print(d[0] * nxpe[0] / nxpe)

    if subideal is not None:
        plt.plot(
            nxpe,
            d[0] * (nxpe[0] / nxpe) ** subideal,
            marker="",
            lw=1,
            linestyle=":",
            color="k",
            label="80\% ideal",
        )

    if False:

        # Functions to test scaling law with nxpe
        t1 = np.array([1024.0 / float(p) for p in nxpe])
        t2 = np.array([np.log(float(p)) for p in nxpe])
        t3 = np.array([1.0 for p in nxpe])
        t4 = np.array([p ** 1.25 for p in nxpe])
        t = t1 + 0.0 * t2 + 0.0 * t3 + 0.0005 * t4

        # t4 = np.array([p**1.25 for p in nxpe])
        # t  = t1 + 0.0*t2 + 0.4*t3 + 0.00006*t4
        t = d[0] * t / t[0]
        # print(t[0])
        # t = 123.53035866666666*t/t[0]

        plt.plot(
            nxpe,
            t,
            marker="",
            lw=2,
            linestyle="-",
            color="g",
        )


def plot_total_time_vs_nxpe(
    args,
    df,
    dfa,
    cdf=None,
    cdfa=None,
    nx=8196,
    rtol=True,
    max_level=None,
    timer="run_time",
    scale="",
):
    """Plot the total run time against nxpe"""

    mls = np.sort(df["max_level"].unique())
    da = []
    danxpe = []
    for i, ml in enumerate(mls):
        d = df[(df.max_level == float(ml)) & (df.nx == float(nx))]

        if (
            len(
                dfa[
                    (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                ].mean_run_time.dropna()
            )
            != 0
        ):
            da.append(
                float(
                    dfa[
                        (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                    ].mean_run_time.dropna()
                )
            )
            ind = (
                dfa[(dfa.max_level == float(ml)) & (dfa.nx == float(nx))]
                .mean_run_time.dropna()
                .index
            )
            danxpe.append(float(dfa[dfa.index == ind[0]].NXPE))

        ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(d, ml, "b", 0, timer)

    danxpe = np.array(danxpe)

    subideal = None
    if nx == 1028:
        subideal = 0.8

    sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(danxpe, da, "b", subideal=subideal)

    if cdf is not None:
        cdff = cdf[cdf.nx == float(nx)]
        ax = sub_plot_cyclic(cdff, timer)

    if cdfa is not None:
        cdfaf = cdfa[cdfa.nx == float(nx)]
        ax = sub_plot_cyclic_average(cdfaf, timer)

    fs = 14
    plt.legend(ncol=2, fontsize=fs - 2, title_fontsize=13)
    plt.xlabel("$N_p$", fontsize=fs)
    ax = set_axis_scale(ax, scale)
    if timer == "run_time":
        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylabel("run time / secs", fontsize=fs)
        if nx == 8196:
            vec = [2, 3, 6, 10, 20, 50, 100, 200]
            ax.set_yticks(vec)
            ax.set_yticklabels(vec, fontsize=fs)
            plt.ylim(2, 200)
            vecx = [16, 32, 64, 128, 256, 512, 1024, 2048]
            ax.set_xticks(vecx)
            ax.set_xticklabels(vecx, fontsize=fs)
        elif nx == 1028:
            vec = [1, 2, 4, 10, 20, 50, 100]
            ax.set_yticks(vec)
            ax.set_yticklabels(vec, fontsize=fs)
            plt.ylim(1, 100)
            vecx = [2, 4, 8, 16, 32, 64, 128, 256, 512]
            ax.set_xticks(vecx)
            ax.set_xticklabels(vecx, fontsize=fs)

            (l1,) = plt.plot([], "bo-", markersize=4)
            (l2,) = plt.plot([], "rs--", markersize=4)
            (l3,) = plt.plot([], "k-", lw=0.5)
            (l4,) = plt.plot([], "k:", lw=1)

            plt.legend(
                [l1, l2, l3, l4],
                ["multigrid-Thomas", "cyclic", "ideal", "80\% ideal"],
                loc=0,
                fontsize=fs - 2,
            )

    plt.grid()
    ax.tick_params(axis="x", which="minor", bottom=False)
    plot_dir = make_plot_dir(args)
    Path(plot_dir).mkdir(parents=True, exist_ok=True)
    if max_level is not None:
        filestr = (
            plot_dir + timer + "_vs_nxpe_max_level_" + str(int(max_level)) + ".pdf"
        )
    else:
        filestr = plot_dir + timer + "_vs_nxpe_nx_" + str(int(nx)) + ".pdf"
    plt.savefig(filestr)
    print(args.open + " " + filestr + " &")
    plt.clf()


def plot_speedup_over_cyclic_vs_nxpe(
    args,
    dfa,
    cdfa,
    max_level=None,
    timer="run_time",
    scale="",
):
    """Plot the speedup in run time over cyclic against nxpe"""

    nxs = np.sort(dfa.nx.unique())

    cstr = "br"
    pstr = "os"
    labelstr = ["$(1024,1024)$", "$(8192,1024)$"]

    fig, ax = plt.subplots()
    axins = ax.inset_axes([0.7, 0.1, 0.25, 0.4])
    plt.axes(axins)

    for j, nx in enumerate(nxs):

        mls = np.sort(dfa["max_level"].unique())
        da = []
        danxpe = []
        ca = []
        for i, ml in enumerate(mls):

            if (
                len(
                    dfa[
                        (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                    ].mean_run_time.dropna()
                )
                != 0
            ):
                da.append(
                    float(
                        dfa[
                            (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                        ].mean_run_time.dropna()
                    )
                )
                ind = (
                    dfa[(dfa.max_level == float(ml)) & (dfa.nx == float(nx))]
                    .mean_run_time.dropna()
                    .index
                )

                nxpe = float(dfa[dfa.index == ind[0]].NXPE)
                danxpe.append(float(nxpe))

                ca.append(
                    float(
                        cdfa[
                            (cdfa.nx == float(nx)) & (cdfa.NXPE == float(nxpe))
                        ].mean_run_time.dropna()
                    )
                )
                # ca.append(float(cdfa[cdfa.nx == float(nx)].mean_run_time))

        danxpe = np.array(danxpe)
        ratio = [100.0 * (ca[i] / da[i] - 1.0) for i, c in enumerate(ca)]

        plt.sca(axins)
        sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(
            danxpe,
            ratio,
            cstr[j],
            subideal=None,
            pointstyle=pstr[j],
            ideal=False,
            labelstr=labelstr[j],
        )
        plt.sca(ax)
        sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(
            danxpe,
            ratio,
            cstr[j],
            subideal=None,
            pointstyle=pstr[j],
            ideal=False,
            labelstr=labelstr[j],
        )

    fs = 14
    plt.legend(title="$(N_x, N_z)$", ncol=2, fontsize=fs - 2, title_fontsize=13)
    plt.xlabel("$N_p$", fontsize=fs)
    ax = set_axis_scale(plt.gca(), scale)
    if timer == "speedup":

        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylabel("relative improvement, $100(T_{CR}/T_{TAMG}-1)$", fontsize=fs)
        vec = [-20, -10, 0, 10, 20, 30, 40, 50]
        ax.set_yticks(vec)
        ax.set_yticklabels(vec, fontsize=fs)
        plt.ylim(-50, 350)
        plt.ylim(-25, 50)
        vecx = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]
        ax.set_xticks(vecx)
        ax.set_xticklabels(vecx, fontsize=fs)

        plt.sca(axins)
        axins = set_axis_scale(plt.gca(), scale)
        vec = [100, 150, 200, 250, 300]  # , 350]
        axins.set_yticks(vec)
        axins.set_yticklabels(vec, fontsize=fs)
        plt.ylim(90, 350)
        vecx = [1024, 2048]
        axins.set_xticks(vecx)
        axins.set_xticklabels(vecx, fontsize=fs)
        plt.xlim(976, 2144)
        plt.sca(axins)
        ax.set_axisbelow(True)
        rectangle = plt.Rectangle((150, -23), 2500, 32, fc="white", ec="white")
        ax.add_patch(rectangle)

    plt.sca(axins)
    plt.grid()
    ax.tick_params(axis="x", which="minor", bottom=False)
    plt.axes(ax)
    plt.grid()
    axins.minorticks_off()
    axins.tick_params(axis="x", which="minor", bottom=False)
    plot_dir = make_plot_dir(args)
    Path(plot_dir).mkdir(parents=True, exist_ok=True)
    if max_level is not None:
        filestr = (
            plot_dir + timer + "_vs_nxpe_max_level_" + str(int(max_level)) + ".pdf"
        )
    else:
        filestr = plot_dir + timer + "_vs_nxpe.pdf"
    plt.savefig(filestr)
    print(args.open + " " + filestr + " &")
    plt.clf()


def plot_efficiency_vs_nxpe(
    args,
    dfa,
    cdfa,
    max_level=None,
    timer="run_time",
    scale="",
):
    """Plot the efficiency in run time against nxpe"""

    nxs = np.sort(dfa.nx.unique())

    cstr = "br"
    pstr = "os"
    labelstr = ["cyclic", "multigrid-Thomas"]

    fig, ax = plt.subplots()

    for j, nx in enumerate(nxs):

        mls = np.sort(dfa["max_level"].unique())
        da = []
        danxpe = []
        ca = []
        for i, ml in enumerate(mls):

            if (
                len(
                    dfa[
                        (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                    ].mean_run_time.dropna()
                )
                != 0
            ):
                da.append(
                    float(
                        dfa[
                            (dfa.max_level == float(ml)) & (dfa.nx == float(nx))
                        ].mean_run_time.dropna()
                    )
                )
                ind = (
                    dfa[(dfa.max_level == float(ml)) & (dfa.nx == float(nx))]
                    .mean_run_time.dropna()
                    .index
                )

                nxpe = float(dfa[dfa.index == ind[0]].NXPE)
                danxpe.append(float(nxpe))

                ca.append(
                    float(
                        cdfa[
                            (cdfa.nx == float(nx)) & (cdfa.NXPE == float(nxpe))
                        ].mean_run_time.dropna()
                    )
                )

        danxpe = np.array(danxpe)
        deff = [
            100.0 * da[0] * danxpe[0] / (da[i] * danxpe[i]) for i, _ in enumerate(da)
        ]
        ceff = [
            100.0 * ca[0] * danxpe[0] / (ca[i] * danxpe[i]) for i, _ in enumerate(da)
        ]

        # Set true to print timing data
        if False:
            print(deff)

        sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(
            danxpe,
            deff,
            cstr[0],
            subideal=None,
            pointstyle=pstr[j],
            ideal=False,
            labelstr=labelstr[j],
        )
        sub_plot_ipt_mean_time_vs_nxpe_fixed_max_level(
            danxpe,
            ceff,
            cstr[1],
            subideal=None,
            pointstyle=pstr[j],
            ideal=False,
            labelstr=labelstr[j],
            linestyle="--",
        )

    ax = set_axis_scale(ax, scale)
    fs = 14
    plt.legend(title="$(N_x, N_z)$", ncol=2, fontsize=fs - 2, title_fontsize=13)

    (l1,) = plt.plot([], "b-")
    (l2,) = plt.plot([], "r--")

    (m1,) = plt.plot([], "bo-", markersize=4)
    (m2,) = plt.plot([], "bs-", markersize=4)

    legend1 = plt.legend(
        [l1, l2], ["multigrid-Thomas", "cyclic"], loc=8, fontsize=fs - 2
    )
    plt.legend(
        [m1, m2],
        ["$(1024,1024)$", "$(8192,1024)$"],
        loc=3,
        title="$(N_x, N_z)$",
        fontsize=fs - 2,
        title_fontsize=13,
    )
    plt.gca().add_artist(legend1)

    plt.xlabel("$N_p$", fontsize=fs)

    plt.xlabel("$N_p$", fontsize=fs)
    plt.ylabel("percentage efficiency, $100\ N_{p0}T_0/(N_pT)$", fontsize=fs)
    vec = [0, 20, 40, 60, 80, 100, 120]
    ax.set_yticks(vec)
    ax.set_yticklabels(vec, fontsize=fs)
    plt.ylim(0, 120)
    vecx = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]
    ax.set_xticks(vecx)
    ax.set_xticklabels(vecx, fontsize=fs)

    plt.grid()
    # ax.minorticks_off()
    ax.tick_params(axis="x", which="minor", bottom=False)
    plot_dir = make_plot_dir(args)
    Path(plot_dir).mkdir(parents=True, exist_ok=True)
    if max_level is not None:
        filestr = (
            plot_dir + timer + "_vs_nxpe_max_level_" + str(int(max_level)) + ".pdf"
        )
    else:
        filestr = plot_dir + timer + "_vs_nxpe.pdf"
    plt.savefig(filestr)
    print(args.open + " " + filestr + " &")
    plt.clf()


def set_axis_scale(ax, scale):
    """Set axis to log scale depending on "scale" variable"""
    if scale == "xlog" or scale == "loglog":
        ax.set_xscale("log")
    else:
        ax.set_xscale("linear")
    if scale == "ylog" or scale == "loglog":
        ax.set_yscale("log")
    else:
        ax.set_yscale("linear")

    return ax
