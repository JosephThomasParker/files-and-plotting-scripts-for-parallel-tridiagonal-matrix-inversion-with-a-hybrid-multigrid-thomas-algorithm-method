#!/usr/bin/env python3

from boututils.run_wrapper import shell, shell_safe
import argparse
import datetime
import glob
import numpy as np
import os
import pandas as pd
import pathlib
import shutil
import timeit
import manuscript_routines as mr


def get_data_from_log_files(log_dir, solver="ipt"):
    """Parse log files and put their data in a pandas DataFrame."""
    df = pd.DataFrame(
        columns=[
            "atol_ipt",
            "rtol_ipt",
            "atol_cvode",
            "rtol_cvode",
            "NXPE",
            "nx",
            "solver_type",
            "run_time",
            "rhs_time",
            "rhs_hits",
            "rhs_time_per_hit",
            "max_level",
            "logfile",
        ]
    )
    first = True

    filenamelist = filter_filenames(log_dir)

    for path_to_logfile in filenamelist:
        with open(path_to_logfile, "r") as f:
            for line_number, line in enumerate(f):
                if line.startswith("BOUT++ version 5.0.0-alpha"):
                    if not first:
                        if solver_type == solver:
                            df = df.append(
                                {
                                    "solver_type": solver_type,
                                    "NXPE": NXPE,
                                    "nx": nx,
                                    "rtol_ipt": rtol_ipt,
                                    "atol_ipt": atol_ipt,
                                    "rtol_cvode": rtol_cvode,
                                    "atol_cvode": atol_cvode,
                                    "run_time": run_time,
                                    "rhs_time": rhs_time,
                                    "rhs_pc": rhs_pc,
                                    "rhs_hits": rhs_hits,
                                    "rhs_time_per_hit": rhs_time_per_hit,
                                    "run_time_per_hit": run_time / rhs_hits,
                                    "invert_time": invert_time,
                                    "invert_pc": invert_pc,
                                    "invert_hits": invert_hits,
                                    "invert_time_per_hit": invert_time_per_hit,
                                    "NOUT": nout,
                                    "max_level": max_level,
                                    "wtime": [wtime],  # NB this is a list
                                    "logfile": path_to_logfile,
                                },
                                ignore_index=True,
                            )
                    else:
                        first = False
                if line.find("phiboussinesq:type = ") != -1:
                    if line.find("phiboussinesq:type = ipt") != -1:
                        if line.find("overwritten") != -1:
                            pass
                        else:
                            solver_type = "ipt"
                    if line.find("phiboussinesq:type = cyclic") != -1:
                        if line.find("overwritten") != -1:
                            pass
                        else:
                            solver_type = "cyclic"
                            rtol_ipt = np.nan
                            atol_ipt = np.nan
                            max_level = np.nan
                if line.startswith("Option nxpe", 1):
                    NXPE = float(line.split()[3])
                if line.startswith("Option NXPE", 1):
                    NXPE = float(line.split()[3])
                if line.startswith("Option mesh:nx", 1):
                    nx = float(line.split()[3])
                if line.startswith("Option solver:rtol", 1):
                    rtol_cvode = float(line.split()[3])
                if line.startswith("Option solver:atol", 1):
                    atol_cvode = float(line.split()[3])
                if line.startswith("Option phiboussinesq:rtol", 1):
                    rtol_ipt = float(line.split()[3])
                if line.startswith("Option phiboussinesq:atol", 1):
                    atol_ipt = float(line.split()[3])
                if line.startswith("Option phiboussinesq:max_level", 1):
                    max_level = float(line.split()[3])
                if line.startswith("Option nout", 1):
                    nout = float(line.split()[3])
                    wtime = np.nan * np.zeros(shape=int(nout) + 1)
                if line.startswith("run        |"):
                    run_time = float(line.split()[2])
                if line.startswith("rhs        |"):
                    rhs_time = float(line.split()[2])
                    rhs_pc = float(line.split()[4][:-1])  # remove % sign
                    rhs_hits = int(line.split()[6])
                    rhs_time_per_hit = float(line.split()[8])
                if line.startswith("invert     |"):
                    invert_time = float(line.split()[2])
                    invert_pc = float(line.split()[4][:-1])  # remove % sign
                    invert_hits = int(line.split()[6])
                    invert_time_per_hit = float(line.split()[8])
                if line.startswith("LaplaceIPT error:"):
                    run_time = np.nan
                    rhs_time = np.nan
                    rhs_hits = np.nan
                    rhs_time_per_hit = np.nan
                if line.startswith("Step", 3):
                    fmttime = line.split()[6]
                    hr = float(fmttime.split(":")[0])
                    mn = float(fmttime.split(":")[1])
                    sec = float(fmttime.split(":")[2])
                    step = int(line.split()[2])
                    wtime[step] = 3600.0 * hr + 60.0 * mn + sec

            # Append final run's data
            if solver_type == solver:
                df = df.append(
                    {
                        "solver_type": solver_type,
                        "NXPE": NXPE,
                        "nx": nx,
                        "rtol_ipt": rtol_ipt,
                        "atol_ipt": atol_ipt,
                        "rtol_cvode": rtol_cvode,
                        "atol_cvode": atol_cvode,
                        "run_time": run_time,
                        "rhs_time": rhs_time,
                        "rhs_pc": rhs_pc,
                        "rhs_hits": rhs_hits,
                        "rhs_time_per_hit": rhs_time_per_hit,
                        "run_time_per_hit": run_time / rhs_hits,
                        "invert_time": invert_time,
                        "invert_pc": invert_pc,
                        "invert_hits": invert_hits,
                        "invert_time_per_hit": invert_time_per_hit,
                        "NOUT": nout,
                        "max_level": max_level,
                        "wtime": [wtime],  # NB this is a list
                        "logfile": path_to_logfile,
                    },
                    ignore_index=True,
                )
            first = True

    return df


def getListOfFiles(dirName):
    """For the given path, get the List of all files in the directory tree"""
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles


def filter_filenames(log_dir):
    filenamelist = []
    for path_to_logfile in getListOfFiles(log_dir):
        filename = os.path.basename(path_to_logfile)
        if filename[0] == ".":
            print("Skipping hidden file: " + filename)
            continue
        if filename[0:5] == "plots":
            continue
        if os.path.isdir(path_to_logfile):
            print("Skipping directory: " + filename)
            # skip directories
            continue
        if not filename[4:6] == ".o":
            print("Skipping non log files (no .o Archer extension): " + filename)
            continue
        filenamelist.append(path_to_logfile)
    return filenamelist


def average_data(df, solver="ipt"):
    dfa = pd.DataFrame(
        columns=[
            "max_level",
            "atol_ipt",
            "rtol_ipt",
            "atol_cvode",
            "rtol_cvode",
            "NXPE",
            "nx",
            "mean_run_time",
            "std_run_time",
            "mean_rhs_time",
            "mean_rhs_pc",
            "mean_rhs_hits",
            "mean_rhs_time_per_hit",
            "ci_up_rhs_time_per_hit",
            "ci_down_rhs_time_per_hit",
            "mean_run_time_per_hit",
            "mean_invert_time",
            "mean_invert_pc",
            "mean_invert_hits",
            "mean_invert_time_per_hit",
        ]
    )

    nxpes = df.NXPE.unique()
    mls = df.max_level.unique()
    nxs = df.nx.unique()
    for nx in nxs:
        for ml in mls:
            for nxpe in nxpes:
                if solver == "ipt":
                    d = df[
                        (df.NXPE == nxpe)
                        & (df.max_level == ml)
                        & (df.solver_type == solver)
                        & (df.nx == nx)
                    ]
                else:
                    d = df[
                        (df.NXPE == nxpe) & (df.solver_type == solver) & (df.nx == nx)
                    ]
                dfa = dfa.append(
                    {
                        "NXPE": nxpe,
                        "nx": nx,
                        "max_level": ml,
                        "rtol_ipt": d.rtol_ipt,
                        "atol_ipt": d.atol_ipt,
                        "rtol_cvode": d.rtol_cvode,
                        "atol_cvode": d.atol_cvode,
                        # Run times
                        "mean_run_time": d.run_time.mean(),
                        "std_run_time": d.run_time.std(),
                        "ci_up_run_time": d.run_time.mean() + 1.96 * d.run_time.std(),
                        "ci_down_run_time": d.run_time.mean() - 1.96 * d.run_time.std(),
                        # RHS times
                        "mean_rhs_time": d.rhs_time.mean(),
                        "std_rhs_time": d.rhs_time.std(),
                        "mean_rhs_hits": d.rhs_hits.mean(),
                        "std_rhs_hits": d.rhs_hits.std(),
                        "ci_up_rhs_hits": d.rhs_hits.mean() + 1.96 * d.rhs_hits.std(),
                        "ci_down_rhs_hits": d.rhs_hits.mean() - 1.96 * d.rhs_hits.std(),
                        "mean_rhs_time_per_hit": d.rhs_time_per_hit.mean(),
                        "std_rhs_time_per_hit": d.rhs_time_per_hit.std(),
                        "ci_up_rhs_time_per_hit": d.rhs_time_per_hit.mean()
                        + 1.96 * d.rhs_time_per_hit.std(),
                        "ci_down_rhs_time_per_hit": d.rhs_time_per_hit.mean()
                        - 1.96 * d.rhs_time_per_hit.std(),
                        "mean_run_time_per_hit": d.run_time_per_hit.mean(),
                        "std_run_time_per_hit": d.run_time_per_hit.std(),
                        "cov_run_time_per_hit": d.run_time_per_hit.std()
                        / d.run_time_per_hit.mean(),
                        "ci_up_run_time_per_hit": d.run_time_per_hit.mean()
                        + 1.96 * d.run_time_per_hit.std(),
                        "ci_down_run_time_per_hit": d.run_time_per_hit.mean()
                        - 1.96 * d.run_time_per_hit.std(),
                        # Invert time
                        "mean_invert_time": d.invert_time.mean(),
                        "std_invert_time": d.invert_time.std(),
                        "mean_invert_time_per_hit": d.invert_time_per_hit.mean(),
                        "std_invert_time_per_hit": d.invert_time_per_hit.std(),
                        "ci_up_invert_time": d.invert_time.mean()
                        + 1.96 * d.invert_time.std(),
                        "ci_down_invert_time": d.invert_time.mean()
                        - 1.96 * d.invert_time.std(),
                        "cov_invert_time_per_hit": d.invert_time_per_hit.std()
                        / d.invert_time_per_hit.mean(),
                        "ci_up_invert_time_per_hit": d.invert_time_per_hit.mean()
                        + 1.96 * d.invert_time_per_hit.std(),
                        "ci_down_invert_time_per_hit": d.invert_time_per_hit.mean()
                        - 1.96 * d.invert_time_per_hit.std(),
                        # Invert time percentage
                        "mean_invert_pc": d.invert_pc.mean(),
                        "std_invert_pc": d.invert_pc.std(),
                        "ci_up_invert_pc": d.invert_pc.mean()
                        + 1.96 * d.invert_pc.std(),
                        "ci_down_invert_pc": d.invert_pc.mean()
                        - 1.96 * d.invert_pc.std(),
                    },
                    ignore_index=True,
                )
    # print(dfa)
    # print(dfa.mean_run_time)
    # print(dfa.loc[:,('mean_run_time','nx','NXPE','max_level')])
    return dfa


def main():
    parser = argparse.ArgumentParser(
        description="parse BOUT++ outfiles and plot timing data"
    )
    parser.add_argument(
        "--max-level", default=None, help="Filter ipt cases only to use this level"
    )
    parser.add_argument(
        "--cyclic", default=None, help="File name of cyclic base case data"
    )
    parser.add_argument(
        "--log-dir", default=None, help="Directory containing output files to plot"
    )
    parser.add_argument(
        "--open",
        default="evince",
        help="Command for opening pdfs. This is only used to produce a copy-and-paste-able prompt",
    )

    args = parser.parse_args()

    log_dir = args.log_dir

    df = None
    dfa = None

    if log_dir is not None:
        df = get_data_from_log_files(log_dir)
        dfa = average_data(df)

    cdf = None
    cdfa = None

    if args.cyclic is not None:
        cdf = get_data_from_log_files(args.cyclic, solver="cyclic")
        cdfa = average_data(cdf, solver="cyclic")

    mr.plot_efficiency_vs_nxpe(args, dfa, cdfa, timer="efficiency", scale="xlog")

    mr.plot_total_time_vs_nxpe(args, df, dfa, cdf, cdfa, 8196, scale="loglog")
    mr.plot_total_time_vs_nxpe(args, df, dfa, cdf, cdfa, 1028, scale="loglog")

    if args.cyclic is not None:
        mr.plot_speedup_over_cyclic_vs_nxpe(
            args, dfa, cdfa, timer="speedup", scale="xlog"
        )

    exit(0)


if __name__ == "__main__":
    main()
