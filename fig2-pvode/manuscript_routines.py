import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pandas as pd

from matplotlib import rc

rc("font", **{"family": "DejaVu Sans", "serif": ["Times"]})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc("text", usetex=True)


def make_plot_dir(args):
    if args.log_dir is not None:
        plot_dir = "plots/" + args.log_dir + "/"
    else:
        plot_dir = "plots/" + args.cyclic + "/"
    return plot_dir


def sub_plot_cyclic(cdf, timer):
    """Plot the data from cyclic runs"""
    # print(cdf.sort_values("NXPE")[[timer, "NXPE"]])
    ax = cdf.sort_values("NXPE").plot(
        x="NXPE",
        y=timer,
        ax=plt.gca(),
        marker="+",
        lw=0,
        color="red",
        label="",
    )

    return ax


def sub_plot_cyclic_average(
    cdfa, timer, confidence_intervals=False, ideal_logn=False, ideal=True
):
    """Plot the averaged data from cyclic runs"""
    ax = cdfa.sort_values("NXPE").plot(
        x="NXPE",
        y="mean_" + timer,
        ax=plt.gca(),
        marker="s",
        lw=1,
        color="red",
        label="cyclic",
        markersize=4,
        linestyle="--",
    )

    if ideal_logn:
        nx = np.sort(cdfa.NXPE.unique())
        n0 = nx[0]
        t0 = float(cdfa[cdfa.NXPE == n0]["mean_" + timer].dropna().min())
        # t_ideal = np.array([t0*np.log(n0)/np.log(n) for n in nx])
        # plt.plot(nx,t_ideal,'--')
        t_ideal = np.array([t0 * n0 * np.log2(n) / np.log2(n0) / n for n in nx])
        t_ideal2 = np.array([t0 * (n0 / n) ** 0.5 for n in nx])
        t1 = np.array([t0 * n0 / n for n in nx])
        t2 = np.array([t0 * (n / n0) ** 2.0 for n in nx])
        # t2 = np.array([t0*np.log(n)*n0/np.log(n0)/n for n in nx])
        t3 = np.array([t0 * np.log(n) / np.log(n0) for n in nx])
        # plt.plot(nx,t_ideal,'k--',lw=1)
        # plt.plot(nx,t_ideal2,'k:',lw=1)
        a = 0.0004
        b = 0.02
        t = (1.0 - a - b) * t1 + a * t2 + b * t3
        t = t0 * t / t[0]
        # plt.plot(nx,(1.0-a-b)*t1,'g-',lw=1)
        # plt.plot(nx,a*t2,'b-',lw=1)
        # plt.plot(nx,b*t3,'r-',lw=1)
        plt.plot(nx, t, "m-", lw=1)

    if ideal:
        nx = np.sort(cdfa.NXPE.unique())
        n0 = nx[0]
        t0 = float(cdfa[cdfa.NXPE == n0]["mean_" + timer].dropna().min())
        t_ideal = np.array([t0 * n0 / n for n in nx])
        plt.plot(nx, t_ideal, "k-", lw=0.5)

    if confidence_intervals:
        ax.fill_between(
            cdfa.sort_values(by="NXPE").NXPE,
            cdfa.sort_values(by="NXPE")["ci_down_" + timer],
            cdfa.sort_values(by="NXPE")["ci_up_" + timer],
            color="grey",
            alpha=0.2,
            lw=0,
        )
    return ax


def sub_plot_ipt_time_vs_nxpe_fixed_max_level(
    d,
    da,
    max_level,
    colorstr,
    i,
    timer="run_time",
    confidence_intervals=False,
    ideal=False,
    ideal_logn=False,
    print_data=False,
):
    if d is not None:
        ax = d.sort_values("NXPE").plot(
            x="NXPE",
            y=timer,
            ax=plt.gca(),
            marker="+",
            lw=0,
            color=[colorstr[i]],
            label="",
        )

    ax = da.plot(
        x="NXPE",
        y="mean_" + timer,
        ax=plt.gca(),
        marker="o",
        lw=1,
        markersize=4,
        color=[colorstr[i]],
        label=str(int(max_level)),
    )

    if ideal:
        nx = np.sort(da.NXPE.unique())
        n0 = nx[0]
        if da.solver_type.any() == "cyclic":
            t0 = float(da[da.NXPE == n0]["mean_" + timer].dropna().min())
            t_ideal = np.array([t0 * n0 / n for n in nx])
            plt.plot(nx, t_ideal, "k-", lw=0.5)

        elif not da[(da.NXPE == n0) & (da.max_level == 4)].empty:
            t0 = float(da[(da.NXPE == n0) & (da.max_level == 4)]["mean_" + timer])
            t_ideal = np.array([t0 * n0 / n for n in nx])
            plt.plot(nx, t_ideal, "k-", lw=0.5)

    if print_data:
        print(da.NXPE)
        if d is not None:
            print(str(d.sort_values("NXPE")[timer]))
        print(str(da["mean_" + timer]))
        if ideal:
            print(t_ideal)

    if ideal_logn:
        nx = np.sort(da.NXPE.unique())
        n0 = nx[0]

        if not da[(da.NXPE == n0) & (da.max_level == 4)].empty:
            t0 = float(da[(da.NXPE == n0) & (da.max_level == 4)]["mean_" + timer])
            t_ideal = np.array([t0 * n0 * np.log2(n) / np.log2(n0) / n for n in nx])
            t_ideal2 = np.array([t0 * (n0 / n) ** 0.5 for n in nx])
            t1 = np.array([t0 * n0 / n for n in nx])
            t2 = np.array([t0 * np.log(n) / np.log(n0) for n in nx])
            # t2 = np.array([t0*np.log(n)*n0/np.log(n0)/n for n in nx])
            t3 = np.array([t0 * (n / n0) ** 2.0 for n in nx])
            t4 = np.array([t0 for n in nx])
            a = 0.01
            b = 0.000035
            c = 0.1
            t = (1.0 - a - b - c) * t1 + a * t2 + b * t3 + c * t4
            t = t0 * t / t[0]
            # plt.plot(nx,(1.0-a-b)*t1,'g-',lw=1)
            # plt.plot(nx,a*t2,'b-',lw=1)
            # plt.plot(nx,b*t3,'r-',lw=1)
            # plt.plot(nx,c*t4,'k-',lw=1)
            plt.plot(nx, t, "m-", lw=1)

    ###    nxpe = np.sort(da.NXPE.unique())
    ###    if not da[(da.NXPE == nxpe[0]) & (da.max_level == 4)].empty:
    ###        t0 = float(da[(da.NXPE == nxpe[0]) & (da.max_level == 4)]['mean_'+timer])
    ###        t1 = np.array([1024./float(p) for p in nxpe])
    ###        t2 = np.array([np.log(float(p)) for p in nxpe])
    ###        t3 = np.array([1.0 for p in nxpe])
    ###        #t4 = np.array([p**1.25 for p in nxpe])
    ###        #t  = t1 + 0.0*t2 + 0.*t3 + 0.0005*t4
    ###
    ###        #t4 = np.array([p**1.25 for p in nxpe])
    ###        #t  = t1 + 0.0*t2 + 0.4*t3 + 0.00006*t4
    ###
    ###        #t4 = np.array([p**2.0 for p in nxpe])
    ###        #t  = t1 + 0.0*t2 + 0.0*t3 + 0.000002*t4
    ###        #t  = t0*t/t[0]
    ###
    ###        t4 = np.array([p**2.0 for p in nxpe])
    ###        t  = t1 + 0.0*t2 + 0.0*t3 + 0.0000002*t4
    ###        t  = t0*t/t[0]
    ###        #print(t[0])
    ###        #t = 123.53035866666666*t/t[0]
    ###
    ###        plt.plot( nxpe, t,
    ###            marker="",
    ###            lw=2,
    ###            linestyle='-',
    ###            color='g',
    ###        )

    if confidence_intervals:
        ax.fill_between(
            da.NXPE,
            da["ci_down_" + timer],
            da["ci_up_" + timer],
            color=[colorstr[i]],
            alpha=0.2,
            lw=0,
        )
    return ax


def plot_total_time_over_cyclic_vs_nxpe(
    args,
    df,
    dfa,
    cdf=None,
    cdfa=None,
    rtol=True,
    max_level=None,
    timer="run_time",
    scale="",
):
    """Plot the total run time over cyclic run time against nxpe"""

    if cdfa is None:
        print("Cannot plot ratio of runtimes without --cyclic specified")
        return

    if max_level is not None:
        if type(max_level) is str:
            max_level = max_level.split(",")
        if type(max_level) is list:
            mlstr = ""
            # cs = plt.cm.jet(np.linspace(0, 1, len(max_level)))
            cs = plt.cm.jet(np.linspace(0, 1, len(max_level)))
            for i, ml in enumerate(max_level):
                d = df[df.max_level == float(ml)]
                da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
                da = calcalate_speed_up(da, cdfa)
                ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                    None, da, ml, cs, i, timer, confidence_intervals=False
                )
                mlstr += str(int(ml))
        else:
            d = df[df.max_level == float(max_level)]
            da = dfa[dfa.max_level == float(max_level)].sort_values("NXPE")
            # cs = plt.cm.jet(np.linspace(0, 1, 1))
            cs = plt.cm.jet(np.linspace(0, 1, 1))
            ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                d, da, max_level, cs, 0, timer
            )
            mlstr = str(int(max_level))
    else:
        mls = np.sort(df["max_level"].unique())
        # cs = plt.cm.jet(np.linspace(0, 1, len(mls)))
        cs = plt.cm.jet(np.linspace(0, 1, len(mls)))
        mlstr = ""
        for i, ml in enumerate(mls):
            d = df[df.max_level == float(ml)]
            da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
            ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(d, da, ml, cs, i, timer)
            mlstr += str(int(ml))

    fs = 14
    plt.legend(ncol=2, title="maximum level", fontsize=fs - 2, title_fontsize=fs - 1)
    ax = set_axis_scale(ax, scale)
    plt.xlabel("nxpe")
    if timer == "run_time":
        plt.ylabel("run time / secs")
    elif timer == "invert_time":
        plt.ylabel("invert time / secs", fontsize=fs)
    elif timer == "rhs_hits":
        plt.ylabel("right-hand side calls")
    elif timer == "run_time_per_hit":
        plt.ylabel("run time per right-hand side call / sec")
        scale = "loglog"
    elif timer == "invert_pc":
        plt.ylabel("invert time as % of total time")
        plt.ylim(0, 100)
    elif timer == "run_time_speed_up_vs_cyclic":
        plt.grid()
        ax.set_xticks([16, 32, 64, 128, 256, 512, 1024, 2048, 4096])
        ax.set_xticklabels([16, 32, 64, 128, 256, 512, 1024, 2048, 4096], fontsize=fs)
        plt.ylim(-50, 100)
        vec = [-50, -40, -20, 0, 20, 40, 60, 80, 100]
        ax.set_yticks(vec)
        ax.set_yticklabels(vec, fontsize=fs)
        ax.minorticks_off()
        plt.xlabel("$N_p$", fontsize=fs)
        # plt.ylabel("percentage speed up, $100(T_{TMG} - T_{CR})/T_{CR}$", fontsize=fs)
        plt.ylabel("relative improvement, $100(T_{CR}/T_{TAMG}-1)$", fontsize=fs)
        plt.yticks(fontsize=fs)
        # plt.ylim(0,100)
    plot_dir = make_plot_dir(args)
    Path(plot_dir).mkdir(parents=True, exist_ok=True)
    if max_level is not None:
        filestr = plot_dir + timer + "_vs_nxpe_max_level_" + mlstr + ".pdf"
    else:
        filestr = plot_dir + timer + "_vs_nxpe.pdf"
    plt.savefig(filestr)
    print(args.open + " " + filestr + " &")
    plt.clf()


def plot_total_time_vs_nxpe(
    args,
    df,
    dfa,
    cdf=None,
    cdfa=None,
    rtol=True,
    max_level=None,
    timer="run_time",
    scale="",
    confidence_intervals=False,
    ideal=False,
    ideal_logn=False,
):
    """Plot the total run time against nxpe"""

    if df is not None or dfa is not None:
        if max_level is not None:
            if type(max_level) is str:
                max_level = max_level.split(",")
            if type(max_level) is list:
                mlstr = ""
                for i, ml in enumerate(max_level):
                    mlstr += str(int(ml))
            else:
                mlstr = str(int(max_level))
        else:
            if df is not None:
                mls = np.sort(df["max_level"].unique())
            else:
                mls = np.sort(dfa["max_level"].unique())
            mlstr = ""
            for i, ml in enumerate(mls):
                mlstr += str(int(ml))

    if df is not None:
        if max_level is not None:
            if type(max_level) is str:
                max_level = max_level.split(",")
            if type(max_level) is list:
                mlstr = ""
                cs = plt.cm.jet(np.linspace(0, 1, len(max_level)))
                for i, ml in enumerate(max_level):
                    d = df[df.max_level == float(ml)]
                    da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
                    ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                        d, da, ml, cs, i, timer, ideal=ideal, ideal_logn=ideal_logn
                    )
                    mlstr += str(int(ml))
            else:
                d = df[df.max_level == float(max_level)]
                da = dfa[dfa.max_level == float(max_level)].sort_values("NXPE")
                # cs = plt.cm.jet(np.linspace(0, 1, 1))
                cs = plt.cm.jet(np.linspace(0, 1, 1))
                ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                    d,
                    da,
                    max_level,
                    cs,
                    0,
                    timer,
                    confidence_intervals=confidence_intervals,
                    ideal_logn=ideal_logn,
                )
                mlstr = str(int(max_level))
        else:
            mls = np.sort(df["max_level"].unique())
            # cs = plt.cm.jet(np.linspace(0, 1, len(mls)))
            cs = plt.cm.jet(np.linspace(0, 1, len(mls)))
            mlstr = ""
            for i, ml in enumerate(mls):
                d = df[df.max_level == float(ml)]
                da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
                ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                    d, da, ml, cs, i, timer, ideal_logn=ideal_logn
                )
                mlstr += str(int(ml))

    if df is None and dfa is not None:
        if max_level is not None:
            if type(max_level) is str:
                max_level = max_level.split(",")
            if type(max_level) is list:
                mlstr = ""
                cs = plt.cm.jet(np.linspace(0, 1, len(max_level)))
                for i, ml in enumerate(max_level):
                    da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
                    ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                        None,
                        da,
                        ml,
                        cs,
                        i,
                        timer,
                        confidence_intervals=confidence_intervals,
                        ideal=ideal,
                        ideal_logn=ideal_logn,
                    )
                    mlstr += str(int(ml))
            else:
                da = dfa[dfa.max_level == float(max_level)].sort_values("NXPE")
                cs = plt.cm.jet(np.linspace(0, 1, 1))
                ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                    None,
                    da,
                    max_level,
                    cs,
                    0,
                    timer,
                    confidence_intervals=confidence_intervals,
                    ideal_logn=ideal_logn,
                )
                mlstr = str(int(max_level))
        else:
            mls = np.sort(dfa["max_level"].unique())
            cs = plt.cm.jet(np.linspace(0, 1, len(mls)))
            mlstr = ""
            for i, ml in enumerate(mls):
                da = dfa[dfa.max_level == float(ml)].sort_values("NXPE")
                ax = sub_plot_ipt_time_vs_nxpe_fixed_max_level(
                    None,
                    da,
                    ml,
                    cs,
                    i,
                    timer,
                    confidence_intervals=confidence_intervals,
                    ideal_logn=ideal_logn,
                )
                mlstr += str(int(ml))

    if cdf is not None:
        ax = sub_plot_cyclic(cdf, timer)

    if cdfa is not None:
        ax = sub_plot_cyclic_average(
            cdfa,
            timer,
            confidence_intervals=confidence_intervals,
            ideal=False,
            ideal_logn=ideal_logn,
        )

    fs = 14
    plt.legend(ncol=2, title="maximum level", fontsize=fs - 2, title_fontsize=13)
    plt.xlabel("$N_p$", fontsize=fs)
    ax = set_axis_scale(ax, scale)
    if timer == "run_time":
        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylabel("run time / secs", fontsize=fs)
        vec = [10, 13, 17, 20, 50, 100, 200, 500]
        ax.set_yticks(vec)
        ax.set_yticklabels(vec, fontsize=fs)
        plt.ylim(10, 600)
    elif timer == "invert_time":
        plt.ylabel("invert time / secs", fontsize=fs)
        plt.yticks(fontsize=fs)
        plt.ylim(0, 60)
    elif timer == "rhs_hits":
        plt.ylabel("right-hand side calls", fontsize=fs)
        plt.yticks(fontsize=fs)
        ax.minorticks_on()
    elif timer == "run_time_per_hit":
        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylabel("run time per right-hand side call / sec", fontsize=fs)
        scale = "loglog"
        vec = [0.006, 0.01, 0.02, 0.05, 0.1, 0.2, 0.3]
        ax.set_yticks(vec)
        ax.set_yticklabels(vec)
        plt.yticks(fontsize=fs)
        plt.ylim(0.006, 0.3)
    elif timer == "invert_pc":
        plt.ylabel("invert time as percentage of total time", fontsize=fs)
        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylim(0, 100)
        plt.yticks(fontsize=fs)
    elif timer == "efficiency":
        plt.xlabel("$N_p$", fontsize=fs)
        plt.ylabel("percentage efficiency, $100\ N_{p0}T_{0}/(N_pT)$", fontsize=fs)
        plt.yticks(fontsize=fs)
    plt.grid()
    ax.set_xticks([32, 64, 128, 256, 512, 1024, 2048, 4096])
    ax.set_xticklabels([32, 64, 128, 256, 512, 1024, 2048, 4096], fontsize=fs)
    # ax.minorticks_off()
    ax.tick_params(axis="x", which="minor", bottom=False)
    plot_dir = make_plot_dir(args)
    Path(plot_dir).mkdir(parents=True, exist_ok=True)
    if max_level is not None and dfa is not None:
        filestr = plot_dir + timer + "_vs_nxpe_max_level_" + mlstr + ".pdf"
    else:
        filestr = plot_dir + timer + "_vs_nxpe.pdf"
    plt.savefig(filestr)
    print(args.open + " " + filestr + " &")
    plt.clf()


def set_axis_scale(ax, scale):
    """Set axis to log scale depending on "scale" variable"""
    if scale == "xlog" or scale == "loglog":
        ax.set_xscale("log")
    else:
        ax.set_xscale("linear")
    if scale == "ylog" or scale == "loglog":
        ax.set_yscale("log")
    else:
        ax.set_yscale("linear")

    return ax


def calcalate_speed_up(da, cdfa):
    for index, row in da.iterrows():
        rt = row.mean_run_time
        ct = float(cdfa[cdfa.NXPE == row.NXPE].mean_run_time)
        da.loc[index, ("mean_run_time_over_cyclic")] = rt / ct
        da.loc[index, ("mean_run_time_speed_up_vs_cyclic")] = 100.0 * (ct - rt) / ct
    return da
