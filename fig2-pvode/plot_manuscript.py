#!/usr/bin/env python3

from boututils.run_wrapper import shell, shell_safe
import argparse
import datetime
import glob
import numpy as np
import os
import pandas as pd
import pathlib
import shutil
import timeit
import manuscript_routines as pr


def get_data_from_log_files(log_dir, solver="ipt"):
    """Parse log files and put their data in a pandas DataFrame."""
    df = pd.DataFrame(
        columns=[
            "atol_ipt",
            "rtol_ipt",
            "atol_cvode",
            "rtol_cvode",
            "NXPE",
            "solver_type",
            "run_time",
            "rhs_time",
            "rhs_hits",
            "rhs_time_per_hit",
            "max_level",
            "logfile",
        ]
    )
    first = True

    for filename in os.listdir(log_dir):
        path_to_logfile = os.path.join(log_dir, filename)
        # skip temporary files
        if filename[0] == ".":
            print("Skipping hidden file: " + filename)
            continue
        if filename[0:5] == "plots":
            continue
        if os.path.isdir(path_to_logfile):
            print("Skipping directory: " + filename)
            # skip directories
            continue
        if not filename[-9:-7] == ".o":
            print("Skipping non log files (no .out extension): " + filename)
            continue
        with open(path_to_logfile, "r") as f:
            for line_number, line in enumerate(f):
                if line.startswith("BOUT++ version 5.0.0-alpha"):
                    if not first:
                        if solver_type == solver:
                            df = df.append(
                                {
                                    "solver_type": solver_type,
                                    "NXPE": NXPE,
                                    "rtol_ipt": rtol_ipt,
                                    "atol_ipt": atol_ipt,
                                    "rtol_cvode": rtol_cvode,
                                    "atol_cvode": atol_cvode,
                                    "run_time": run_time,
                                    "rhs_time": rhs_time,
                                    "rhs_pc": rhs_pc,
                                    "rhs_hits": rhs_hits,
                                    "rhs_time_per_hit": rhs_time_per_hit,
                                    "run_time_per_hit": run_time / rhs_hits,
                                    "invert_time": invert_time,
                                    "invert_pc": invert_pc,
                                    "invert_hits": invert_hits,
                                    "invert_time_per_hit": invert_time_per_hit,
                                    "comms_time": comms_time,
                                    "comms_pc": comms_pc,
                                    "comms_hits": comms_hits,
                                    "comms_time_per_hit": comms_time_per_hit,
                                    "NOUT": nout,
                                    "max_level": max_level,
                                    "wtime": [wtime],  # NB this is a list
                                    "logfile": path_to_logfile,
                                },
                                ignore_index=True,
                            )
                    else:
                        first = False
                if line.find("phiboussinesq:type = ") != -1:
                    if line.find("phiboussinesq:type = ipt") != -1:
                        if line.find("overwritten") != -1:
                            pass
                        else:
                            solver_type = "ipt"
                    if line.find("phiboussinesq:type = cyclic") != -1:
                        if line.find("overwritten") != -1:
                            pass
                        else:
                            solver_type = "cyclic"
                            rtol_ipt = np.nan
                            atol_ipt = np.nan
                            max_level = np.nan
                if line.startswith("Option nxpe", 1):
                    NXPE = float(line.split()[3])
                if line.startswith("Option NXPE", 1):
                    NXPE = float(line.split()[3])
                if line.startswith("Option solver:rtol", 1):
                    rtol_cvode = float(line.split()[3])
                if line.startswith("Option solver:atol", 1):
                    atol_cvode = float(line.split()[3])
                if line.startswith("Option phiboussinesq:rtol", 1):
                    rtol_ipt = float(line.split()[3])
                if line.startswith("Option phiboussinesq:atol", 1):
                    atol_ipt = float(line.split()[3])
                if line.startswith("Option phiboussinesq:max_level", 1):
                    max_level = float(line.split()[3])
                if line.startswith("Option nout", 1):
                    nout = float(line.split()[3])
                    wtime = np.nan * np.zeros(shape=int(nout) + 1)
                if line.startswith("run        |"):
                    run_time = float(line.split()[2])
                if line.startswith("rhs        |"):
                    rhs_time = float(line.split()[2])
                    rhs_pc = float(line.split()[4][:-1])  # remove % sign
                    rhs_hits = int(line.split()[6])
                    rhs_time_per_hit = float(line.split()[8])
                if line.startswith("invert     |"):
                    invert_time = float(line.split()[2])
                    invert_pc = float(line.split()[4][:-1])  # remove % sign
                    invert_hits = int(line.split()[6])
                    invert_time_per_hit = float(line.split()[8])
                if line.startswith("comms      |"):
                    comms_time = float(line.split()[2])
                    comms_pc = float(line.split()[4][:-1])  # remove % sign
                    comms_hits = int(line.split()[6])
                    comms_time_per_hit = float(line.split()[8])
                if line.startswith("LaplaceIPT error:"):
                    run_time = np.nan
                    rhs_time = np.nan
                    rhs_hits = np.nan
                    rhs_time_per_hit = np.nan
                if line.startswith("Step", 3):
                    fmttime = line.split()[6]
                    hr = float(fmttime.split(":")[0])
                    mn = float(fmttime.split(":")[1])
                    sec = float(fmttime.split(":")[2])
                    step = int(line.split()[2])
                    wtime[step] = 3600.0 * hr + 60.0 * mn + sec

            # Append final run's data
            if solver_type == solver:
                df = df.append(
                    {
                        "solver_type": solver_type,
                        "NXPE": NXPE,
                        "rtol_ipt": rtol_ipt,
                        "atol_ipt": atol_ipt,
                        "rtol_cvode": rtol_cvode,
                        "atol_cvode": atol_cvode,
                        "run_time": run_time,
                        "rhs_time": rhs_time,
                        "rhs_pc": rhs_pc,
                        "rhs_hits": rhs_hits,
                        "rhs_time_per_hit": rhs_time_per_hit,
                        "run_time_per_hit": run_time / rhs_hits,
                        "invert_time": invert_time,
                        "invert_pc": invert_pc,
                        "invert_hits": invert_hits,
                        "invert_time_per_hit": invert_time_per_hit,
                        "comms_time": comms_time,
                        "comms_pc": comms_pc,
                        "comms_hits": comms_hits,
                        "comms_time_per_hit": comms_time_per_hit,
                        "NOUT": nout,
                        "max_level": max_level,
                        "wtime": [wtime],  # NB this is a list
                        "logfile": path_to_logfile,
                    },
                    ignore_index=True,
                )
            first = True

    return df


def average_data(df, solver="ipt"):
    dfa = pd.DataFrame(
        columns=[
            "solver_type",
            "max_level",
            "atol_ipt",
            "rtol_ipt",
            "atol_cvode",
            "rtol_cvode",
            "NXPE",
            "mean_run_time",
            "std_run_time",
            "mean_rhs_time",
            "mean_rhs_pc",
            "mean_rhs_hits",
            "mean_rhs_time_per_hit",
            "ci_up_rhs_time_per_hit",
            "ci_down_rhs_time_per_hit",
            "mean_run_time_per_hit",
            "mean_invert_time",
            "mean_invert_pc",
            "mean_invert_hits",
            "mean_invert_time_per_hit",
        ]
    )

    nxpes = df.NXPE.unique()
    mls = df.max_level.unique()
    ats = df.atol_ipt.unique()
    rts = df.rtol_ipt.unique()
    for rt in rts:
        for at in ats:
            for ml in mls:
                for nxpe in nxpes:
                    if solver == "ipt":
                        d = df[
                            (df.NXPE == nxpe)
                            & (df.max_level == ml)
                            & (df.solver_type == solver)
                            & (df.atol_ipt == at)
                            & (df.rtol_ipt == rt)
                        ]
                    else:
                        d = df[(df.NXPE == nxpe) & (df.solver_type == solver)]
                    dfa = dfa.append(
                        {
                            "solver_type": solver,
                            "NXPE": nxpe,
                            "max_level": ml,
                            "rtol_ipt": rt,
                            "atol_ipt": at,
                            "rtol_cvode": d.rtol_cvode,
                            "atol_cvode": d.atol_cvode,
                            # Run times
                            "mean_run_time": d.run_time.mean(),
                            "std_run_time": d.run_time.std(),
                            "ci_up_run_time": d.run_time.mean()
                            + 1.96 * d.run_time.std(),
                            "ci_down_run_time": d.run_time.mean()
                            - 1.96 * d.run_time.std(),
                            # RHS times
                            "mean_rhs_time": d.rhs_time.mean(),
                            "std_rhs_time": d.rhs_time.std(),
                            "mean_rhs_hits": d.rhs_hits.mean(),
                            "std_rhs_hits": d.rhs_hits.std(),
                            "ci_up_rhs_hits": d.rhs_hits.mean()
                            + 1.96 * d.rhs_hits.std(),
                            "ci_down_rhs_hits": d.rhs_hits.mean()
                            - 1.96 * d.rhs_hits.std(),
                            "mean_rhs_time_per_hit": d.rhs_time_per_hit.mean(),
                            "std_rhs_time_per_hit": d.rhs_time_per_hit.std(),
                            "ci_up_rhs_time_per_hit": d.rhs_time_per_hit.mean()
                            + 1.96 * d.rhs_time_per_hit.std(),
                            "ci_down_rhs_time_per_hit": d.rhs_time_per_hit.mean()
                            - 1.96 * d.rhs_time_per_hit.std(),
                            "mean_run_time_per_hit": d.run_time_per_hit.mean(),
                            "std_run_time_per_hit": d.run_time_per_hit.std(),
                            "cov_run_time_per_hit": d.run_time_per_hit.std()
                            / d.run_time_per_hit.mean(),
                            "ci_up_run_time_per_hit": d.run_time_per_hit.mean()
                            + 1.96 * d.run_time_per_hit.std(),
                            "ci_down_run_time_per_hit": d.run_time_per_hit.mean()
                            - 1.96 * d.run_time_per_hit.std(),
                            # Invert time
                            "mean_invert_time": d.invert_time.mean(),
                            "std_invert_time": d.invert_time.std(),
                            "mean_invert_time_per_hit": d.invert_time_per_hit.mean(),
                            "std_invert_time_per_hit": d.invert_time_per_hit.std(),
                            "ci_up_invert_time": d.invert_time.mean()
                            + 1.96 * d.invert_time.std(),
                            "ci_down_invert_time": d.invert_time.mean()
                            - 1.96 * d.invert_time.std(),
                            "cov_invert_time_per_hit": d.invert_time_per_hit.std()
                            / d.invert_time_per_hit.mean(),
                            "ci_up_invert_time_per_hit": d.invert_time_per_hit.mean()
                            + 1.96 * d.invert_time_per_hit.std(),
                            "ci_down_invert_time_per_hit": d.invert_time_per_hit.mean()
                            - 1.96 * d.invert_time_per_hit.std(),
                            # Invert time percentage
                            "mean_invert_pc": d.invert_pc.mean(),
                            "std_invert_pc": d.invert_pc.std(),
                            "ci_up_invert_pc": d.invert_pc.mean()
                            + 1.96 * d.invert_pc.std(),
                            "ci_down_invert_pc": d.invert_pc.mean()
                            - 1.96 * d.invert_pc.std(),
                            # Comms time
                            "mean_comms_time": d.comms_time.mean(),
                            "std_comms_time": d.comms_time.std(),
                            "mean_comms_time_per_hit": d.comms_time_per_hit.mean(),
                            "std_comms_time_per_hit": d.comms_time_per_hit.std(),
                            "ci_up_comms_time": d.comms_time.mean()
                            + 1.96 * d.comms_time.std(),
                            "ci_down_comms_time": d.comms_time.mean()
                            - 1.96 * d.comms_time.std(),
                            "cov_comms_time_per_hit": d.comms_time_per_hit.std()
                            / d.comms_time_per_hit.mean(),
                            "ci_up_comms_time_per_hit": d.comms_time_per_hit.mean()
                            + 1.96 * d.comms_time_per_hit.std(),
                            "ci_down_comms_time_per_hit": d.comms_time_per_hit.mean()
                            - 1.96 * d.comms_time_per_hit.std(),
                            # Comms time percentage
                            "mean_comms_pc": d.comms_pc.mean(),
                            "std_comms_pc": d.comms_pc.std(),
                            "ci_up_comms_pc": d.comms_pc.mean()
                            + 1.96 * d.comms_pc.std(),
                            "ci_down_comms_pc": d.comms_pc.mean()
                            - 1.96 * d.comms_pc.std(),
                        },
                        ignore_index=True,
                    )
    # if dfa.solver_type.any()=="cyclic":

    dfa = calculate_efficiency(dfa)

    return dfa


def calculate_efficiency(d):
    # t0 = float(d.loc[d['NXPE'].idxmin()].mean_run_time)
    n0 = float(d.loc[d["NXPE"].idxmin()].NXPE)
    if d.solver_type.any() == "cyclic":
        t0 = float(d[d.NXPE == n0].mean_run_time.dropna().min())
    else:
        t0 = float(d[(d.NXPE == n0) & (d.max_level == 4)].mean_run_time)
    for index, row in d.iterrows():
        t = float(row.mean_run_time)
        n = float(row.NXPE)
        d.loc[index, ("mean_efficiency")] = 100.0 * n0 * t0 / (n * t)
    return d


def plot_vs_nxpe(
    args,
    df,
    dfa,
    cdf,
    cdfa,
    timer="run_time",
    scale="",
    confidence_intervals=False,
    ideal=False,
    ideal_logn=False,
):
    """Plot generic timers against nxpe"""

    if args.max_level is not None:
        pr.plot_total_time_vs_nxpe(
            args,
            df,
            dfa,
            cdf,
            cdfa,
            max_level=args.max_level,
            timer=timer,
            scale=scale,
            confidence_intervals=confidence_intervals,
            ideal=ideal,
            ideal_logn=ideal_logn,
        )
    else:
        mls = np.sort(df["max_level"].unique())
        for ml in mls:
            pr.plot_total_time_vs_nxpe(
                args,
                df,
                dfa,
                cdf,
                cdfa,
                max_level=ml,
                timer=timer,
                scale=scale,
                ideal=ideal,
                ideal_logn=ideal_logn,
            )
    pr.plot_total_time_vs_nxpe(
        args,
        df,
        dfa,
        cdf,
        cdfa,
        timer=timer,
        scale=scale,
        confidence_intervals=confidence_intervals,
        ideal=ideal,
        ideal_logn=ideal_logn,
    )


def main():
    parser = argparse.ArgumentParser(
        description="parse BOUT++ outfiles and plot timing data"
    )
    parser.add_argument(
        "--max-level",
        default=None,
        help="Filter ipt cases only to use these levels (comma-separated string)",
    )
    parser.add_argument(
        "--cyclic", default=None, help="File name of cyclic base case data"
    )
    parser.add_argument(
        "--log-dir", default="logs", help="Directory containing output files to plot"
    )
    parser.add_argument(
        "--open",
        default="evince",
        help="Command for opening pdfs. This is only used to produce a copy-and-paste-able prompt",
    )

    args = parser.parse_args()

    log_dir = args.log_dir

    df = None
    dfa = None

    if log_dir is not None:
        df = get_data_from_log_files(log_dir)
        dfa = average_data(df)

    cdf = None
    cdfa = None

    if args.cyclic is not None:
        cdf = get_data_from_log_files(args.cyclic, solver="cyclic")
        cdfa = average_data(cdf, solver="cyclic")

    plot_vs_nxpe(
        args,
        None,
        dfa,
        None,
        cdfa,
        timer="efficiency",
        scale="xlog",
        confidence_intervals=False,
    )

    plot_vs_nxpe(
        args, df, dfa, cdf, cdfa, timer="run_time_per_hit", scale="loglog", ideal=True
    )

    plot_vs_nxpe(args, df, dfa, cdf, cdfa, timer="rhs_hits", scale="xlog")

    plot_vs_nxpe(
        args,
        df,
        dfa,
        cdf,
        cdfa,
        timer="invert_pc",
        scale="xlog",
        ideal_logn=False,
        ideal=False,
    )

    # Plot total times
    pr.plot_total_time_vs_nxpe(
        args, df, dfa, cdf, cdfa, max_level=args.max_level, scale="loglog", ideal=True
    )

    pr.plot_total_time_over_cyclic_vs_nxpe(
        args,
        df,
        dfa,
        cdf,
        cdfa,
        max_level=args.max_level,
        scale="xlog",
        timer="run_time_speed_up_vs_cyclic",
    )

    # Plot best case (level 4) only
    if False:
        pr.plot_total_time_vs_nxpe_tmg_only(
            args, df, dfa, max_level="4", scale="", ideal=True
        )
        pr.plot_total_time_vs_nxpe_tmg_only(
            args,
            None,
            dfa,
            max_level="4",
            scale="",
            ideal=False,
            timer="efficiency",
        )

    exit(0)


if __name__ == "__main__":
    main()
