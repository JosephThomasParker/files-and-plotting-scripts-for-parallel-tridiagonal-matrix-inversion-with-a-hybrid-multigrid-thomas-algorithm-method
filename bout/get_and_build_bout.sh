#!/bin/bash

git clone https://github.com/boutproject/BOUT-dev.git
cd BOUT-dev
git checkout 276f77758e18c24bfcbf3e9b6e356930437df6f8
module swap PrgEnv-cray PrgEnv-gnu/5.2.82
module load fftw/3.3.4.11
module load cray-netcdf/4.4.1.1
module load cray-hdf5/1.10.0.1
./configure --enable-checks=no --enable-optimize=3
make
cd examples/blob2d
make
