all: fig1 fig2

fig1:
	cd fig1-rk4 && make

fig2:
	cd fig2-pvode && make

clean: 
	cd fig1-rk4 && make clean
	cd fig2-pvode && make clean

