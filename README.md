# Input files, output files and plotting scripts for ``Parallel tridiagonal matrix inversion with a hybrid multigrid--Thomas algorithm method''

This archive contains the files required to reproduce the results presented in _Parallel tridiagonal matrix inversion with a hybrid multigrid--Thomas algorithm method_, J. T. Parker, P. A. Hill, D. Dickinson and B. D. Dudson.  

A list of files is given in the [manifest](#manifest) below.

### Reproducing plots

Create and activate a virtual python environment, and install the dependencies

```
python3 -m venv plotenv
. plotenv/bin/activate
pip install -r requirements.txt
```

To produce the plots from the manuscript, invoke `make` in the top directory or in `fig*` subdirectories.
Plots are saved in the directory `fig*/plots/<data_folder_name>`.

### Reproducing simulation data

Output files are produced using [BOUT++](http://boutproject.github.io/), which is available on [GitHub](https://github.com/boutproject/BOUT-dev) using git commit `276f77758e18c24bfcbf3e9b6e356930437df6f8`. 
Executing the script `get_and_build_bout.sh` in the `bout` directory will checkout and build the correct revision of BOUT++, using the Archer modules that we used.
This produces the `blob2d` executable in the directory `bout/BOUT-dev/examples/blob2d`. 
The submission scripts in the directories `fig*/job_submission` point to this executable. 


### Manifest
```
├── bout
│   └── get_and_build_bout.sh
├── fig1-rk4  
│   ├── cyclic  
│   │   └── Archer output files for cyclic reduction algorithm, used in plotting
│   ├── ipt
│   │   └── Archer output files for multigrid/Thomas algorithm, used in plotting
│   ├── job_submission
│   │   ├── 1024x1024
│   │   │   ├── Archer submission scripts
│   │   │   └── delta_0.25
│   │   │       └── BOUT++ input file
│   │   └── 8192x1024
│   │       ├── Archer submission scripts
│   │       └── delta_0.25
│   │           └── BOUT++ input file
│   ├── Makefile
│   ├── manuscript_routines.py
│   ├── plot_manuscript.py
│   └── plots
│       └── ipt
│           └── Output figures
├── fig2-pvode
│   ├── atol_1e-7
│   │   └── Archer output files for both algorithms, used in plotting
│   ├── job_submission
│   │   ├── Archer submission scripts
│   │   └── delta_0.25
│   │       └── BOUT++ input file
│   ├── Makefile
│   ├── manuscript_routines.py
│   ├── plot_manuscript.py
│   └── plots
│       └── atol_1e-7
│           └── Output figures
├── Makefile
├── README.md
└── requirements.txt
```
